<?php


    function admin_init()
    {
        register_setting('shanti_social_network', "facebook_page_link", [
            'type' => 'string',
            'sanitize_callback' => 'sanitize_facebook_field',
            'default' => "#",
        ]);

        register_setting('shanti_social_network', "instagram_page_link", [
            'type' => 'string',
            'sanitize_callback' => 'sanitize_text_field',
            'default' => "#",
        ]);
    }

    function admin_menu()
    {
        add_options_page('Social media', 'Social media', 'manage_options', 'social-media', 'admin_page');
    }


    function admin_page()
    {
        ?>
        <div class="wrap">
            <h1>Social media</h1>
            <form method="post" action="options.php" style="max-width: 768px">
                <?php settings_fields('shanti_social_network'); ?>
                <?php do_settings_sections('shanti_social_network'); ?>
                <?php foreach ([
                    'facebook_page_link' =>  'facebook_page_link',
                    "instagram_page_link" => "instagram_page_link"
                ] as $key=>$label): ?>
                    <div style="margin-bottom: 1rem;">

                        <label><?= $label ?></label>
                        <input  class="widefat" type="text" placeholder="<?= $label ?>" name="<?= $key ?>" value="<?php echo esc_attr(get_option($key)); ?>" />
                    </div>
                <?php endforeach; ?>
                <?php submit_button(__('Sauvegarder les modifications', 'shanti')); ?>

            </form>
        </div>
        <?php
    }

if (is_admin()) {
    add_action('admin_init', 'admin_init') ;
    add_action('admin_menu', 'admin_menu');
    // apply_filters('sanitize_text_field', $filtered, $str);
}


function sanitize_facebook_field($text)
{
    return $text."-----";
}
