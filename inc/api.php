<?php


// all apis need to be inside wp rest_api_init action
add_action('rest_api_init', function () {

    //http://youdomaine.com/wp-json/optima/v1/client/list
    register_rest_route("optima/v1", "/client/list", array(
        'methods' => "GET",
        'accept_json' => true,
        'callback' => "api_client_list",
        'permission_callback' => function () {
            return '';
        }
    ));


    register_rest_route("optima/v1", "/client/remove", array(
        'methods' => "POST",
        'accept_json' => true,
        'callback' => "api_client_remove",
        'permission_callback' => function () {
            return '';
        }
    ));
});


function api_client_list()
{
    $json = file_get_contents("https://fakestoreapi.com/products/1");
    return $json;
}



function api_client_remove($request)
{
    // $posts = get_posts();
    // return json_encode($posts);
}
