<?php
/*
  * Template Name: Contact Page
*/



get_header();
?>
<div class="row justify-content-center">
    <div class="col-10">
        <h1 class="text-danger"><?= the_title() ?></h1>
        <h1><?= the_content() ?></h1>
    </div>
</div>

<?php
get_footer();
