<?php

require_once __DIR__    ."/inc/api.php" ;
require_once __DIR__    ."/inc/social.php" ;



//our cool theme does support menus
add_theme_support('menus');


// register nav menu place
register_nav_menu('nav_menu', 'Header Navigation');
register_nav_menu('footer_menu', __('Footer Navigation'));


// safe way to add your stylesheets and script to theme
wp_enqueue_script("optima-admin-js", get_template_directory_uri() . '/assets/js/app.js', array( 'jquery' ));
wp_enqueue_style("optima-main-css", get_template_directory_uri() . '/assets/css/app.css');



$json = json_encode([
    "a" => "A",
    "b" => "B",
]);

// passing a variable from php to javascript
wp_localize_script('optima-admin-js', 'optima_data', $json);
