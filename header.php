<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Optima </title>
    <meta http-equiv="X-UA-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body <?php body_class() ?>>
<?php
    wp_nav_menu(
    [
            'theme_location' => 'nav_menu',
            'menu_class' => 'navbar-nav d-flex list-style-none',
            'container' => 'ul',
            'fallback_cb' => false,
            'add_li_class' => 'c-white text-uppercase'
        ]
);
                      ?>