# OPTIMA WP THEME

if you want to extend in theme developing see this link 👉👉 [https://developer.wordpress.org/themes/](https://developer.wordpress.org/themes/)

## Plugins

- ### Customisation

  - #### Advanced Custom Fields for WordPress :

    this plugin help you to create custom inputs inside any wp entity.
    [https://www.advancedcustomfields.com/](https://www.advancedcustomfields.com/)

  - #### Custom Post Type UI :

    this plugin help you to create custom posts, taxonomies.
    [https://fr.wordpress.org/plugins/custom-post-type-ui/](https://fr.wordpress.org/plugins/custom-post-type-ui/)

- ### Localization

  - #### Polylang :

    [https://fr.wordpress.org/plugins/polylang/](https://fr.wordpress.org/plugins/polylang/)

  - #### Loco Translate :

    [https://fr.wordpress.org/plugins/loco-translate/](https://fr.wordpress.org/plugins/loco-translate/)

- ### Misc

  - #### Sucuri Security – Audit, analyse de programmes malveillants et renforcement de sécurité :

    [https://fr.wordpress.org/plugins/sucuri-scanner/](https://fr.wordpress.org/plugins/sucuri-scanner/)

  - #### Yoast SEO :

    [https://fr.wordpress.org/plugins/wordpress-seo/](https://fr.wordpress.org/plugins/wordpress-seo/)

## Contact Me

[bilelbho94@gmail.com](mailto:bilelbho94@gmail.com)
