<?php
    // Default post template
    get_header();
?>

<div class="row justify-content-center">
    <div class="col-10">
        <h1><?= the_title() ?></h1>
        <p><?= the_content() ?></p>
    </div>
</div>

<?php
    get_footer();
